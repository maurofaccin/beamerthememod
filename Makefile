TEXDIR=$(HOME)/texmf/tex/latex/mod
DOCDIR=$(HOME)/texmf/doc/latex/mod


build:
	sleep 1

install: build
	mkdir -p $(TEXDIR)
	cp *.sty $(TEXDIR)

